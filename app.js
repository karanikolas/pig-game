/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

//Variables 
var scores, roundScore, activePlayer, gamePlaying;
//This function set everything back to 0
init();


document.querySelector('.btn-roll').addEventListener('click', function(){

        //Generate a random number  between  1 - 6 
        var dice = Math.floor(Math.random() * 6) + 1;
        //Then display the result 
             var diceDOM = document.querySelector('.dice');
                console.log(diceDOM);
                 diceDOM.style.display ='block';
                 diceDOM.src = 'dice-' + dice +'.png';
        //Update the round score if the rolled number was not 1
        if(dice !== 1 ){
            //add the score
            //rouncscore = roundscore + dice; it's the same
            roundScore += dice;
            document.querySelector('#current-' + activePlayer).textContent = roundScore;
    
        }else{ 
            nextPlayer();
        }   
    });


document.querySelector('.btn-hold').addEventListener('click' , function(){
            //When the hold button is clicked
        //Add the user score to the global score
        scores[activePlayer] += roundScore;
        
        //Update UI 
        document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];

        //Check if player won the game
        if(scores[activePlayer] >= 100){
            document.querySelector('#name-' + activePlayer).textContent = 'Winner!';
            document.querySelector('.dice').style.display = 'none';
            document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
            document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
            gamePlaying = false;
        }else{
        //Call next player
        nextPlayer();
        }
    
})

function nextPlayer(){
 //Next Player
     //Next player
     activePlayer === 0 ? activePlayer =1 : activePlayer =0;
     roundScore = 0;
     //set the score back to  0 
     document.getElementById('current-0').textContent = '0';
     document.getElementById('current-1').textContent = '0';

     //Set the active sign for the active player

     document.querySelector('.player-0-panel').classList.toggle('active');
     document.querySelector('.player-1-panel').classList.toggle('active');

     // document.querySelector('.player-0-panel').classList.remove('active');
     // document.querySelector('.player-1-panel').classList.add('active');

     //RESET the dice image
     document.querySelector('.dice').style.display = 'none';
}

document.querySelector('.btn-new').addEventListener('click', init );

function init(){
    scores=[0,0];
    roundScore = 0;
    activePlayer = 0;

    //Hide the dice
    document.querySelector('.dice').style.display = 'none';
    //initialize score to zero
    document.getElementById('score-0').textContent = '0';
    document.getElementById('score-1').textContent = '0';
    //initialize current-score to zero
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-0').textContent = '0';

    //Change the winner back to player
    document.getElementById('name-0').textContent = 'Player 1';
    document.getElementById('name-1').textContent = 'Player 2';

    //Remove the winner class for bold text
    document.querySelector('.player-0-panel').classList.remove('winner');
    document.querySelector('.player-1-panel').classList.remove('winner');

    //Remove the active class
    document.querySelector('.player-0-panel').classList.remove('active');    
    document.querySelector('.player-1-panel').classList.remove('active');
    document.querySelector('.player-0-panel').classList.add('active');
    
}